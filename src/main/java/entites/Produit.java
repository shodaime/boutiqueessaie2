package entites;


import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Produit {
    private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;
    private Categorie categorie;
    
    public Produit() {}
    public Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption, Categorie cat) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
        this.categorie = cat;
    }
    
   
    public long getId() {return id;}
    public void setId(long id) {this.id = id;}
    public String getLibelle() {return libelle;}
    public void setLibelle(String libelle) {this.libelle = libelle;}
    public double getPrixUnitaire() {return prixUnitaire;}
    public void setPrixUnitaire(double prixUnitaire) {this.prixUnitaire = prixUnitaire;}
    public LocalDate getDatePeremption() {return datePeremption;}
    public void setDatePeremption(LocalDate datePeremption) {this.datePeremption = datePeremption;}
    public Categorie getCategorie() {return categorie;}
    public void setCategorie(Categorie categorie) {this.categorie = categorie;}
    
    public boolean estPerime(){
        Date date = new Date();
        return this.datePeremption.getYear()>date.getYear();
    }
    public boolean estPerime(LocalDate ref){
        return this.datePeremption.getYear()>ref.getYear();
    }
    @Override
    public int hashCode() {
        int hash = 17;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + Objects.hashCode(this.libelle);
        hash = 23 * hash + Objects.hashCode(this.prixUnitaire);
        hash = 23 * hash + Objects.hashCode(this.categorie);
        hash = 23 * hash + Objects.hashCode(this.datePeremption);
        return hash;
    }
    @Override
    public String toString() {
        return "Produit\n id=" + id + ",\n libelle=" + libelle + ",\n prixUnitaire=" + prixUnitaire + ",\n datePeremption=" + datePeremption + ",\n categorie=" + categorie;
    }    
}
