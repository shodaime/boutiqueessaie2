package entites;
        
import java.util.Objects;

public class ProduitAchete {
    private Produit produit;
    private int quantite = 1;
    private double remise = 0.0;
    
    public ProduitAchete() {}
    public ProduitAchete(Produit produit, int quantite, double remise) {
        this.produit = produit;
        this.quantite = quantite;
        this.remise = remise;
    }
    
    public Produit getProduit() {return produit;}
    public void setProduit(Produit produit) {this.produit = produit;}
    public int getQuantite() {return quantite;}
    public void setQuantite(int quantite) {this.quantite = quantite;}
    public double getRemise() {return remise;}
    public void setRemise(double remise) {this.remise = remise;}
    
    public double getPrixTotal(){
        return this.produit.getPrixUnitaire() * quantite *(1 - remise);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.produit.getId());
        hash = 31 * hash + Objects.hashCode(this.quantite);
        hash = 31 * hash + Objects.hashCode(this.remise);
        return hash;
    }
    
    @Override
    public String toString() {
        return "ProduitAchete\n  produit=" + produit.getId() + ", quantite=" + quantite + ", remise=" + remise;
    }
}
