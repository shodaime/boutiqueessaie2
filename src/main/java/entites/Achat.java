package entites;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

public class Achat {
    private long id;
    private double remise = 0.0;
    private LocalDate dateAchat;
    private ArrayList<ProduitAchete> achatList;
    
    public Achat(){}
    
    public Achat(long id, double remise, LocalDate dateAchat, ArrayList<ProduitAchete> achatList) {
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.achatList = achatList;
    }
    

    public long getId(){
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public ArrayList<ProduitAchete> getAchatList() {
        return achatList;
    }

    public void setAchatList(ArrayList<ProduitAchete> achatList) {
        this.achatList = achatList;
    }
    public void addAchatList(ProduitAchete achete){
        this.achatList.add(achete);
    }
    public void removeAchatList(int index){
        this.achatList.remove(index);
    }

    public double getPrixTotal() {
        double price = 0;
        for(ProduitAchete i : achatList){
            price += i.getPrixTotal();
        }
        return price * (1-remise);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.dateAchat);
        hash = 23 * hash + Objects.hashCode(this.achatList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
         if (obj == null || this.getClass()!=obj.getClass() ) {
            return false;
        }
        final Achat other = (Achat) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", achatList=" + achatList + '}';
    }
    
}
