package entites;

import java.time.LocalDate;
import java.util.Objects;

public class Employe extends Personne{
    private String cnss;
    public Employe(){super();}
     public Employe(long id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
    }
    
    public Employe(long id, String nom, String prenom, LocalDate dateNaissance, String cnss) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
    }
    public String getCnss() {return cnss;}
    public void setCnss(String cnss) {this.cnss = cnss;}


    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.cnss);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass()!=obj.getClass() ) {
            return false;
        }
        final Employe other = (Employe) obj;
        return this.id == other.id;
    }
    
    @Override
    public String toString() {
        return "Employe\n identifiant= " + this.getId() + ",\n nom= " + this.getNom() + ",\n prenoms= " + this.getPrenom() + ",\n dateNaissance= " + this.getDateNaissance()+ ",\n cnss=" + cnss ;
    }
    
}
