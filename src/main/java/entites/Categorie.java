package entites;
import java.util.Objects;
public class Categorie {
    private long id;
    private String libelle;
    private String description;

    public Categorie() {
    }
    public Categorie(long id, String libelle, String description) {
        this.id = id;
        this.libelle = libelle;
        this.description = description;
    }

    public long getId() {return id;}
    public void setId(long id) {this.id = id;}
    public String getLibelle() {return libelle;}
    public void setLibelle(String libelle) {this.libelle = libelle;}
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}
    
    @Override
    public int hashCode() {
        int hash = 11;
        hash = 73 * hash + Objects.hashCode(this.id);
        hash = 73 * hash + Objects.hashCode(this.libelle);
        hash = 73 * hash + Objects.hashCode(this.description);
        return hash;
    }
    
    @Override
    public String toString() {
        return "Categorie\n id=" + id + ",\n libelle=" + libelle + ",\n description=" + description;
    }
}
